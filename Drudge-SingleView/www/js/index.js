/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */



var app = {
	
    
	
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        
        
        if(window.innerWidth < 768) {
            ShowFirst();
        }
        
        if(window.innerWidth >= 768) {
            $('.navbar').css("display", "none");
            ShowAll();
        } else {
            $('.navbar').css("display", "block");
            $('#TopButton').addClass("active");
            ShowFirst();
        }
        
        
        $( window ).resize(function() {
                           if(window.innerWidth >= 768) {
                           $('.navbar').css("display", "none");
                           ShowAll();   
                           } else {
                           $('.navbar').css("display", "block");
                           ShowFirst();
                           }
                           
                           });
        
        
        $("#TopButton").click(function() {
                              $('#LeftButton').removeClass("active");
                              $('#MidButton').removeClass("active");
                              $('#RightButton').removeClass("active");
                              $('#TopButton').addClass("active");
                              
                              $('html, body').animate({
                                                      scrollTop: $("#topAnch").offset().top
                                                      }, 500);
                              savedPos = "top";
                              });
        
        $("#LeftButton").click(function() {
                               $('#LeftButton').addClass("active");
                               $('#MidButton').removeClass("active");
                               $('#RightButton').removeClass("active");
                               $('#TopButton').removeClass("active");
                               
                               ShowFirst();
                               $('html, body').animate({
                                                       scrollTop: $("#leftHandCol").offset().top-50
                                                       }, 500);
                               savedPos = "left";
                               });
        
        $("#MidButton").click(function() {
                              $('#LeftButton').removeClass("active");
                              $('#MidButton').addClass("active");
                              $('#RightButton').removeClass("active");
                              $('#TopButton').removeClass("active");
                              ShowSecond();
                              $('html, body').animate({
                                                      scrollTop: $("#middleCol").offset().top-50
                                                      }, 500);
                              savedPos = "mid";
                              });
        
        $("#RightButton").click(function() {
                                $('#LeftButton').removeClass("active");
                                $('#MidButton').removeClass("active");
                                $('#RightButton').addClass("active");
                                $('#TopButton').removeClass("active");
                                ShowThird();
                                $('html, body').animate({
                                                        scrollTop: $("#rightHandCol").offset().top-50
                                                        }, 500);
                                savedPos = "right";
                                });
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
       
    },
    
  
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        $(".app").hide() ;
   
        window.analytics.startTrackerWithId('UA-451855-9');    
	    window.analytics.trackView('Home Page');
       
        
        getContent();
        
    
      
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

    }
};

function ShowFirst() {
    $('#leftHandCol').css("display", "block");
    $('#middleCol').css("display", "none");
    $('#rightHandCol').css("display", "none");
    
    
}

function ShowSecond() {
    $('#leftHandCol').css("display", "none");
    $('#middleCol').css("display", "block");
    $('#rightHandCol').css("display", "none");
    
    
}

function ShowThird() {
    $('#leftHandCol').css("display", "none");
    $('#middleCol').css("display", "none");
    $('#rightHandCol').css("display", "block");
    
    
}

function ShowAll() {
    $('#leftHandCol').css("display", "block");
    $('#middleCol').css("display", "block");
    $('#rightHandCol').css("display", "block");
    
}

function ShowHidden() {
    $('#topAd').css("display", "block");
    $('#tColAd').css("display", "block");
    $('#mtColAd').css("display", "block");
    $('#mbColAd').css("display", "block");
    $('#rtColAd').css("display", "block");
    $('#rbColAd').css("display", "block");
    
    
    
    
    $('#loadingimg').css("display", "none");
}

function Refresh() {
    getContent();
}

function TopAnch() {
    window.location.href = "#topAnch";
}


function RemoveBRS( divs ) {
    //alert(divs.length);
    
    for ( var i = 0; i < 8; i++ ) {
        divs[ i ].remove();
    }
   
}

function compareTimeForReload (pageTime, now, cacheTime)
{
    var diffMs = now - pageTime;
    
    if ( diffMs > cacheTime){
        return true;
    }
    else {
        return false;
    }
    
}

function clearCacheAndRefresh() {
  //  alert ("Clear cache");
    window.localStorage.removeItem("cache");
    getContent();

    
}

// Create the XHR object.
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {
    // XHR for Chrome/Firefox/Opera/Safari.
    xhr.open(method, url, true);
  } else if (typeof XDomainRequest != "undefined") {
    // XDomainRequest for IE.
    xhr = new XDomainRequest();
    xhr.open(method, url);
  } else {
    // CORS not supported.
    xhr = null;
  }
  return xhr;
}


// Make the actual CORS request.
function makeCorsRequest(url, divName, append, cacheName) {
 
  var xhr = createCORSRequest('GET', url);
  if (!xhr) {
    alert('CORS not supported');
    return;
  }

  // Response handlers.
  xhr.onload = function() {
    var text = xhr.responseText;
    
    if (append == false) {
        $(divName).html (text);
    }
    else {
    	 $(divName).append (text);
    }
    
      if  (cacheName.name == "topHeadlines") {
        
          makeCorsRequest('http://www.headlines.plus/Column1.html', "#leftColContent", false, headlinesColumn1);
          makeCorsRequest('http://www.headlines.plus/Column2.html', "#midColContent", false, headlinesColumn2);
          makeCorsRequest('http://www.headlines.plus/Column3.html', "#rightColContent", false, headlinesColumn3);
      }
      
    else if  (cacheName.name == "headlinesColumn1"){
         makeCorsRequest('http://www.headlines.plus/SourcesColumn1.html', "#leftColSources", false, sources1);
    }
    else if  (cacheName.name == "headlinesColumn2"){
         makeCorsRequest('http://www.headlines.plus/SourcesColumn2.html', "#midColSources", false, sources2);
    }
    else if  (cacheName.name == "headlinesColumn3"){
         makeCorsRequest('http://www.headlines.plus/SourcesColumn3.html', "#rightColSources", false, sources3 );
    }

      
    cacheName.content = $(divName).html() ;
    updateCacheObject (cacheName);
      
      
      $("img").addClass("img-responsive");
          
    ShowHidden();
//    alert (text);
//    document.getElementById("cors").innerHTML = text;
  };

  xhr.onerror = function() {
    alert('Woops, there was an error making the request.');
  };

  xhr.send();
}

    
var  topCenterHeadline, headlinesColumn1, headlinesColumn2,headlinesColumn3, sources1,sources2,sources3; 

var topHeadlines = {
	  name:"topHeadlines",
      content: ""
};

var drudgeSiteLogo = {
	  name:"drudgeSiteLogo",
      content: ""
};

var topCenterHeadline = {
	  name:"topCenterHeadline",
      content: ""
};

var headlinesColumn1 = {
	  name:"headlinesColumn1",
      content: ""
};

var headlinesColumn2 = {
	  name:"headlinesColumn2",
      content: ""
};

var headlinesColumn3 = {
	  name:"headlinesColumn3",
      content: ""
};

var sources1 = {
	  name:"sources1",
      content: ""
};
var sources2 = {
	  name:"sources2",
      content: ""
};

var sources3 = {
	  name:"sources3",
      content: ""
};


var object = {topHeadlines : "", 
	    		drudgeSiteLogo : "",
			   	topCenterHeadline : "", 
			   	headlinesColumn1 : "", 
			   	headlinesColumn2 : "",
			   	headlinesColumn3 : "", 
			   	sources1 : "",
			   	sources2 : "",
			   	sources3 : "",
			    timestamp: new Date().getTime()
			    };


function updateCacheObject (cacheName) {

		// probably a clever way of doing this but I can't work it out
		
		if  (cacheName.name == "topHeadlines"){ 	
   	    	object.topHeadlines =cacheName.content;
		}
		
		else if  (cacheName.name == "drudgeSiteLogo"){
   	    	object.drudgeSiteLogo =cacheName.content;
		}
		
		else if  (cacheName.name == "topCenterHeadline"){ 	
   	    	object.topCenterHeadline =cacheName.content;
		}
		
		else if  (cacheName.name == "headlinesColumn1"){ 	
   	    	object.headlinesColumn1 =cacheName.content;
		}
		else if  (cacheName.name == "headlinesColumn2"){ 	
   	    	object.headlinesColumn2 =cacheName.content;
		}
		else if  (cacheName.name == "headlinesColumn3"){ 	
   	    	object.headlinesColumn3 =cacheName.content;
		}

		else if  (cacheName.name == "sources1"){ 	
   	    	object.sources1 =cacheName.content;
		}
	
		else if  (cacheName.name == "sources2"){ 	
   	    	object.sources2 =cacheName.content;
		}

		else if  (cacheName.name == "sources3"){ 	
   	    	object.sources3 =cacheName.content;
		}
	    window.localStorage.setItem("cache", JSON.stringify(object));
}

function useCacheContent (  divName, append, text) {
	if (append == false) {
	    $(divName).html (text);
   	}
    else {
    	 $(divName).append (text);
    }
}

function getContent() {
    var cacheObject =window.localStorage.getItem("cache");
    {
        makeCorsRequest('http://www.headlines.plus/Column0.html', "#top", false, topHeadlines);
	    makeCorsRequest('http://www.headlines.plus/HeadlinesPlusLogo.html', "#logo", false, drudgeSiteLogo);
	    makeCorsRequest('http://www.headlines.plus/MainHeadline.html', "#midheadline", false, topCenterHeadline);
	    
    }
    return;
}

