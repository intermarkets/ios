//
//  CustomWebView.h
//  Drudge-SingleView
//
//  Created by Jon Wetherall on 29/07/2015.
//  Copyright (c) 2015 Jon Wetherall. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CustomDelegate <NSObject>

    -(void)myCustomDelegateMethod;
@end

@interface CustomWebView : UIWebView <UIWebViewDelegate>
{
    id<CustomDelegate> delegate;
    //the rest of the stuff
}
@end
